﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidColorMask : MonoBehaviour
{
    public SpriteRenderer maskSprite;
    public enum Colores {Yellow, Blue, Green, Red, Pink};
    public Colores colorSelector;
    public List<Sprite> AsteroidSprites;

    private List<Color> _colors = new List<Color> { Color.yellow, Color.cyan, Color.green, Color.red, Color.magenta };
    private List<LayerMask> _layers = new List<LayerMask> { 10, 11, 12, 13, 14 };
    private void Awake()
    {
        maskSprite.enabled = false;
        selectColor();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Mask") && collision.GetComponent<MaskController>().inUse)
        {
            maskSprite.enabled = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Mask"))
        {
            maskSprite.enabled = false ;
        }
    }

    private void selectColor()
    {
        switch (colorSelector)
        {
            case Colores.Yellow:
                maskSprite.color = _colors[0];
                gameObject.layer = _layers[0];
                maskSprite.sprite = AsteroidSprites[0];
                GetComponent<SpriteRenderer>().sprite = AsteroidSprites[0];
                break;
            case Colores.Blue:
                maskSprite.color = _colors[1];
                gameObject.layer = _layers[1];
                maskSprite.sprite = AsteroidSprites[1];
                GetComponent<SpriteRenderer>().sprite = AsteroidSprites[1];
                break;
            case Colores.Green:
                maskSprite.color = _colors[2];
                gameObject.layer = _layers[2];
                maskSprite.sprite = AsteroidSprites[2];
                GetComponent<SpriteRenderer>().sprite = AsteroidSprites[2];
                break;
            case Colores.Red:
                maskSprite.color = _colors[3];
                gameObject.layer = _layers[3];
                maskSprite.sprite = AsteroidSprites[3];
                GetComponent<SpriteRenderer>().sprite = AsteroidSprites[3];
                break;
            case Colores.Pink:
                maskSprite.color = _colors[4];
                gameObject.layer = _layers[4];
                maskSprite.sprite = AsteroidSprites[4];
                GetComponent<SpriteRenderer>().sprite = AsteroidSprites[4];
                break;
        }
    }
}
