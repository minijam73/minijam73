﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowColor : MonoBehaviour
{
    private void Awake()
    {
        InvokeRepeating(nameof(Activate), 0.5f, 0.5f);
    }
    void Update()
    {
        GetComponent<SpriteRenderer>().color = ColorChanger.instance.currentColor;
    }

    public void Activate()
    {
        GetComponent<SpriteRenderer>().enabled = true;
    }
}
