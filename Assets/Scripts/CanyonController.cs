﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CanyonController : MonoBehaviour
{
    [Header("Key Controls 0")]
    public KeyCode rightKey0;
    public KeyCode leftKey0;
    public KeyCode shootKey0;

    [Header("Key Controls 1")]
    public KeyCode rightKey1;
    public KeyCode leftKey1;
    public KeyCode shootKey1;
    
    private KeyCode rightKey;
    private KeyCode leftKey;
    private KeyCode shootKey;

    [Header ("Speeds")]
    public float rotSpeed;
    public float shootDelay;
    private float _shootDelay;
    private bool canShoot = true;

    [Header ("References")]
    public GameObject target;
    public GameObject bullet;

    [Header ("Shoot Points")]
    public Transform shootPoint1;
    public Transform shootPoint2;
    public Transform shootPoint3;
    //public Transform shootPoint2;

    [Header("Shake Camera")]
    public float shakeDuration;
    public Vector3 shakeIntensity;

    [Header("PowerUps")]
    public bool TripleShooting = false;



    private float shootTimer;

    private int layerInt;
    private Color color;

    private void Awake()
    {
        _shootDelay = shootDelay;
    }

    private void Update()
    {
        RotateCanyon();
        Shoot();
    }

    public void SetLayer(int layerIndex, Color col)
    {
        layerInt = layerIndex;
        color = col;
    }

    void RotateCanyon()
    {
        if (Input.GetKey(rightKey))
        {
            transform.RotateAround(target.transform.position, Vector3.back, rotSpeed * Time.deltaTime);
        }

        if (Input.GetKey(leftKey))
        {
            transform.RotateAround(target.transform.position, Vector3.forward, rotSpeed * Time.deltaTime);
        }
    }

    void Shoot()
    {
        if (Input.GetKey(shootKey) && canShoot)
        {

            GameObject cam = Camera.main.gameObject;
            cam.transform.DOShakePosition(shakeDuration, shakeIntensity);
            var bull1 = Instantiate(bullet, shootPoint1.position, shootPoint1.rotation);
            bull1.layer = layerInt;

            if (TripleShooting)
            {
                var bull2 = Instantiate(bullet, shootPoint2.position, shootPoint2.rotation);
                bull2.layer = layerInt;
                var bull3 = Instantiate(bullet, shootPoint3.position, shootPoint3.rotation);
                bull3.layer = layerInt;
            }

            SFXController.instance.PlayShoot();
            shootTimer = 0;
            canShoot = false;
        }

        if (!canShoot)
        {
            shootTimer += Time.deltaTime;
            if (shootTimer >= shootDelay) { canShoot = true; }
        }
    }

    public void BulletSpeed_PU(float time)
    {
        shootDelay /= 2;
        Invoke(nameof(BulletSpeed_Over), time);
    }
    public void BulletSpeed_Over()
    {
        shootDelay = _shootDelay;
    }

    public void TripleShot_PU(float time)
    {
        TripleShooting = true;
        Invoke(nameof(TripleShot_Over), time);
    }

    public void TripleShot_Over()
    {
        TripleShooting = false;
    }

    public void ChangeControls(int index)
    {
        if(index == 0)
        {
            leftKey = leftKey0;
            rightKey = rightKey0;
            shootKey = shootKey0;
        }

        if(index == 1)
        {
            leftKey = leftKey1;
            rightKey = rightKey1;
            shootKey = shootKey1;
        }
    }
}
