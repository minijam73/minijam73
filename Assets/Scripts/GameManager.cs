﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    /*
    public float FadeOutTime = 0.75f;
    public Animator FadeOutAnimator;
    */
    [Header("Main Menu")]
    public GameObject mainMenuPanel;
    public GameObject settingsPanel;
    public GameObject creditsPanel;
    public GameObject howToPanel;
    public float timeToOpenPanels;
    public GameObject menuAsteroids;
    public GameObject controls0BG;
    public GameObject controls1BG;
    public GameObject howToControls0;
    public GameObject howToControls1;

    [Header("Game")]
    public GameObject spaceShip;
    public GameObject canyon;
    public GameObject healthCanvas;
    public GameObject powerCanvas;
    public GameObject asteroidsParent;
    public GameObject SpawnerPowerUps;

    [Header("Ingame Menu")]
    public GameObject scorePanel;
    public GameObject pausePanel;
    public GameObject pauseControls0BG;
    public GameObject pauseControls1BG;
    public GameObject endGamePanel;

    private bool isPlaying = false;

    private void Awake()
    {
        ChangeControls(0);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) { PauseGame(); }
    }

    /*
    public void LoadNextScene()
    {
        if (FadeOutAnimator != null)
            FadeOutAnimator.SetTrigger("FadeOut");
        Invoke(nameof(LoadingNS), FadeOutTime);
    }

    private void LoadingNS()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    */

    public void StartGame()
    {
        RestartGame();

        mainMenuPanel.SetActive(false);
        menuAsteroids.SetActive(false);

        spaceShip.SetActive(true);
        healthCanvas.SetActive(true);
        powerCanvas.SetActive(true);
        scorePanel.SetActive(true);
        SpawnerPowerUps.SetActive(true);

        isPlaying = true;

        Timer.instance.StartTimer();
    }

    public void PauseGame()
    {
        if (isPlaying)
        {
            Time.timeScale = 0;

            pausePanel.SetActive(true);
            isPlaying = false;
        }
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;

        pausePanel.SetActive(false);
        isPlaying = true;
    }

    public void SurrenderGame()
    {
        Time.timeScale = 1;

        pausePanel.SetActive(false);
        spaceShip.GetComponent<SpaceshipHealth>().DoDie();
    }

    public void RestartGame()
    {
        spaceShip.GetComponent<SpaceshipHealth>().InitializeSpaceship();
        endGamePanel.SetActive(false);
        ClearAsteroids();
    }

    void ClearAsteroids()
    {
        foreach (Transform child in asteroidsParent.transform)
        {
            Destroy(child.gameObject);
        }
    }

    public void ReturnToMenu()
    {
        ClearAsteroids();

        spaceShip.SetActive(false);
        healthCanvas.SetActive(false);
        powerCanvas.SetActive(false);
        endGamePanel.SetActive(false);

        mainMenuPanel.SetActive(true);
        menuAsteroids.SetActive(true);
    }

    public void CloseGame()
    {
        Application.Quit();
    }

    public void OpenSettings()
    {
        settingsPanel.transform.DOScaleX(1f, timeToOpenPanels)
            .SetEase(Ease.InQuad)
            .SetUpdate(true)
            .Play();
    }

    public void CloseSettings()
    {
        settingsPanel.transform.DOScaleX(0f, timeToOpenPanels)
            .SetEase(Ease.InQuad)
            .SetUpdate(true)
            .Play();
    }

    public void OpenCredits()
    {
        creditsPanel.transform.DOScaleX(1f, timeToOpenPanels)
            .SetEase(Ease.InQuad)
            .SetUpdate(true)
            .Play();
    }

    public void CloseCredits()
    {
        creditsPanel.transform.DOScaleX(0f, timeToOpenPanels)
            .SetEase(Ease.InQuad)
            .SetUpdate(true)
            .Play();
    }

    public void OpenHowTo()
    {
        howToPanel.transform.DOScaleX(1f, timeToOpenPanels)
            .SetEase(Ease.InQuad)
            .SetUpdate(true)
            .Play();
    }

    public void CloseHowTo()
    {
        howToPanel.transform.DOScaleX(0f, timeToOpenPanels)
            .SetEase(Ease.InQuad)
            .SetUpdate(true)
            .Play();
    }

    public void ChangeControls(int index)
    {
        spaceShip.GetComponent<SpaceshipController>().ChangeControls(index);
        canyon.GetComponent<CanyonController>().ChangeControls(index);

        if (index == 0)
        {
            controls1BG.SetActive(false);
            controls0BG.SetActive(true);

            pauseControls1BG.SetActive(false);
            pauseControls0BG.SetActive(true);

            howToControls1.SetActive(false);
            howToControls0.SetActive(true);
        }
        if (index == 1)
        {
            controls0BG.SetActive(false);
            controls1BG.SetActive(true);

            pauseControls0BG.SetActive(false);
            pauseControls1BG.SetActive(true);

            howToControls0.SetActive(false);
            howToControls1.SetActive(true);
        }
    }
}
