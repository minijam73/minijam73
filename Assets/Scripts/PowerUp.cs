﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public enum Ups {DoubleSpeed, TripleShot, Health, DoubleScore}
    public Ups PowerUpType;
    public ScoreController Scorer;
    public CanyonController Canyon;
    public SpaceshipHealth Health;
    public float duration;
    public GameObject HUD;

    private void Awake()
    {
        if(Scorer == null)
            Scorer = GameObject.FindObjectOfType<ScoreController>();
        if(Canyon == null)
            Canyon = GameObject.FindObjectOfType<CanyonController>();
        if(Health == null)
            Health = GameObject.FindObjectOfType<SpaceshipHealth>();
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Spaceship"))
        {
            CheckPUp();
            SFXController.instance.PlayPUp();
            if(HUD != null)
                HUD.SetActive(true);
            gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    public void CheckPUp()
    {
        switch (PowerUpType)
        {
            case Ups.DoubleScore:
                Scorer.DoubleMult(duration);
                break;
            case Ups.Health:
                Health.HealOne();
                break;
            case Ups.TripleShot:
                Canyon.TripleShot_PU(duration);
                break;
            case Ups.DoubleSpeed:
                Canyon.BulletSpeed_PU(duration);
                break;
        }
    }
}
