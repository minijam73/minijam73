﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM_Alien : MonoBehaviour
{
    [Header("Properties")]
    public float speed = 5;
    public float shotDelay = 2;
    public Transform ShotSpawn;
    public GameObject Bullet;
    public float DistanceToShoot;
    public float ZigZagMoveDistance;
    public float Health;
    public int baseScore;
    public enum State {GetCloser, Shot, Tiny_move}
    public State _currentState = State.GetCloser;

    private Vector2 _zigzagdir;
    private Transform _target;
    private float _timerShot;
    private bool _canShoot = true;
    private Transform _parent;

    private void Start()
    {
        _target = GameObject.FindObjectOfType<SpaceshipController>().gameObject.transform;
        _parent = GameObject.FindGameObjectWithTag("AsteroidParent").transform;
        SFXController.instance.PlayAlienAppear();
    }

    // Update is called once per frame
    void Update()
    {
        switch (_currentState)
        {
            case State.GetCloser:
                transform.position = Vector3.MoveTowards(transform.position, _target.position, 0.01f);
                //transform.Translate(transform.forward * speed * Time.deltaTime);
                if(Vector2.Distance(_target.position, transform.position) < DistanceToShoot)
                {
                    _currentState = State.Shot;
                    _timerShot = 0;
                    _canShoot = true;
                }
                break;
            case State.Shot:
                //transform.;
                if (_canShoot)
                {
                    Instantiate(Bullet, ShotSpawn.position, ShotSpawn.rotation, _parent);
                    _canShoot = false;
                }

                _timerShot += Time.deltaTime;
                if(Vector2.Distance(_target.position, transform.position) > DistanceToShoot*1.1)
                {
                    _currentState = State.GetCloser;
                }
                else if( _timerShot >= shotDelay/4)
                {
                    _currentState = State.Tiny_move;
                    int _direction = Random.Range(-1, 2);
                    _zigzagdir = (_target.position - transform.position).normalized * _direction;
                    _zigzagdir = Vector2.Perpendicular(_zigzagdir);
                    _timerShot = 0;
                }
                break;
            case State.Tiny_move:
                transform.Translate(_zigzagdir * speed * Time.deltaTime);
                //transform.position = Vector3.MoveTowards(transform.position, _zigzagTarget.position, 0.01f);
                _timerShot += Time.deltaTime;

                if (Vector3.Distance(_target.position, transform.position) < 0.1 || _timerShot >= 3*shotDelay/4)
                {
                    _currentState = State.Shot;
                    _timerShot = 0;
                    _canShoot = true;
                }
                else
                    Debug.Log(Vector3.Distance(_target.position, transform.position));
                break;
        }
    }

    public void GetDmg()
    {
        SFXController.instance.PlayAlienHit();
        Health--;
        if (Health <= 0)
        {
            ScoreController.instance.IncreaseScore(baseScore);
            AsteroidSpawner.instance.InvokeNewAlien();
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Spaceship"))
        {
            collision.GetComponent<SpaceshipHealth>().TakeDamage();
        }
        else if(collision.CompareTag("Bullet"))
        {
            GetDmg();
            Destroy(collision.gameObject);
            SFXController.instance.PlayHit();
        }
    }
}
