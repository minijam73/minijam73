﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipHealth : MonoBehaviour
{
    public int health;

    public float invulnerableTime;
    public float flickerInterval;

    public GameObject healthCanvas;
    public GameObject endGameCanvas;

    public GameObject scorePanel;

    private float currentHealth;
    private bool canGetHit;
    private SpriteRenderer spriteRenderer;
    private Vector2 startPos;
    private SpaceshipController spaceship;

    private void Awake()
    {
        currentHealth = health;
        spriteRenderer = GetComponent<SpriteRenderer>();
        canGetHit = true;
        startPos = transform.position;
        spaceship = GetComponent<SpaceshipController>();
    }

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.P)) { TakeDamage(); }
    }

    public void InitializeSpaceship()
    {
        transform.position = startPos;
        transform.rotation = Quaternion.identity;
        
        currentHealth = health;
        canGetHit = true;

        foreach (Transform child in healthCanvas.transform)
        {
            child.gameObject.SetActive(true);
        }
    }

    public void TakeDamage()
    {
        if (canGetHit)
        {
            currentHealth--;
            canGetHit = false;

            SFXController.instance.PlayHit();

            InvokeRepeating(nameof(Flicker), 0, flickerInterval);
            Invoke(nameof(EnableHits), invulnerableTime);

            UpdateHealthCanvas();

            if (currentHealth <= 0)
            {
                DoDie();
            }
        }
    }

    void UpdateHealthCanvas()
    {
        foreach (Transform child in healthCanvas.transform)
        {
            if (child.gameObject.activeSelf)
            {
                child.gameObject.SetActive(false);
                return;
            }
        }
    }
    void UpdateHealthPositive()
    {
        foreach (Transform child in healthCanvas.transform)
        {
            if (!child.gameObject.activeSelf)
            {
                child.gameObject.SetActive(true);
                return;
            }
        }
    }

    public void DoDie()
    {
        AsteroidSpawner.instance.StopSpawn();
        ScoreController.instance.SetFinalScore();
        Timer.instance.SetFinalTime();
        spaceship.StopSpaceship();
        SpawnerPowerUps.instance.DeactivateAll();

        Debug.Log("Dead");
        scorePanel.SetActive(false);
        gameObject.SetActive(false);
        endGameCanvas.SetActive(true);
        SpawnerPowerUps.instance.gameObject.SetActive(false);

    }

    private void EnableHits()
    {
        spriteRenderer.color = ColorChanger.instance.currentColor;
        canGetHit = true;
        CancelInvoke(nameof(Flicker));
    }

    private void Flicker()
    {
        if (spriteRenderer.color == Color.white)
        {
            spriteRenderer.color = ColorChanger.instance.currentColor;
        }
        else
        {
            spriteRenderer.color = Color.white;
        }
    }
    public void HealOne()
    {
        currentHealth += 1;
        if (currentHealth > 5)
        {
            currentHealth = 5;
        }
        UpdateHealthPositive();
    }
}
