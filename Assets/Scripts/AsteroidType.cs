﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AsteroidType
{
    Normal, Helix, Spiral
}
