﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;


public class SettingsMenu : MonoBehaviour
{
    public static SettingsMenu settingsMenu;

    public AudioMixer audioMixer;

    public Slider masterSliderMenu;
    public Slider masterSliderGame;
    public Slider musicSliderMenu;
    public Slider musicSliderGame;
    public Slider effectSliderMenu;
    public Slider effectSliderGame;

    private float currentMasterValue = 0.65f;
    private float currentMusicValue = 0.65f;
    private float currentEffectsValue = 0.65f;

    private void Awake()
    {
        if (!settingsMenu) { settingsMenu = this; }
        else { Destroy(gameObject); }
    }

    private void Start()
    {
        SetSlidersValue();
    }

    public void setMasterVolume(float v)
    {
        audioMixer.SetFloat("MasterVolume", Mathf.Log10(v) * 20);
        currentMasterValue = v;

        SetSlidersValue();
    }

    public void setMusicVolume(float v)
    {
        audioMixer.SetFloat("MusicVolume", Mathf.Log10(v) * 20);
        currentMusicValue = v;

        SetSlidersValue();
    }

    public void setEffectsVolume(float v)
    {
        audioMixer.SetFloat("EffectsVolume", Mathf.Log10(v) * 20);
        currentEffectsValue = v;

        SetSlidersValue();
    }

    public void SetSlidersValue()
    {
        masterSliderMenu.value = currentMasterValue;
        masterSliderGame.value = currentMasterValue;

        musicSliderMenu.value = currentMusicValue;
        musicSliderGame.value = currentMusicValue;

        effectSliderMenu.value = currentEffectsValue;
        effectSliderGame.value = currentEffectsValue;
    }

}
