﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SpaceshipController : MonoBehaviour
{
    [Header("Key Controls 0")]
    public KeyCode frontKey0;
    public KeyCode rightKey0;
    public KeyCode leftKey0;

    [Header("Key Controls 1")]
    public KeyCode frontKey1;
    public KeyCode rightKey1;
    public KeyCode leftKey1;

    private KeyCode frontKey;
    private KeyCode rightKey;
    private KeyCode leftKey;

    [Header ("Speeds")]
    public float moveSpeed;
    public float rotSpeed;

    [Header ("Acceleration & Inertia")]
    public float accelerationIncreaseValue;
    public float intertiaDecreaseValue;

    private float currentMoveSpeed;

    private void Update()
    {
        MoveForward();
        RotateSpaceship();
    }

    void MoveForward()
    {
        if (Input.GetKey(frontKey))
        {
            if(currentMoveSpeed < moveSpeed) { currentMoveSpeed += accelerationIncreaseValue * Time.deltaTime; }
            else if(currentMoveSpeed > moveSpeed) { currentMoveSpeed = moveSpeed; }
        }
        else
        {
            if(currentMoveSpeed > 0) { currentMoveSpeed -= intertiaDecreaseValue * Time.deltaTime; }
            else if(currentMoveSpeed < 0) { currentMoveSpeed = 0; }
        }

        transform.Translate(Vector2.up * currentMoveSpeed * Time.deltaTime);
    }

    public void StopSpaceship() { currentMoveSpeed = 0; }

    void RotateSpaceship()
    {
        if (Input.GetKey(rightKey))
        {
            transform.Rotate(Vector3.back * rotSpeed * Time.deltaTime);
        }

        if (Input.GetKey(leftKey))
        {
            transform.Rotate(Vector3.forward * rotSpeed * Time.deltaTime);
        }
    }

    public void ChangeControls(int index)
    {
        if (index == 0)
        {
            leftKey = leftKey0;
            rightKey = rightKey0;
            frontKey = frontKey0;
        }

        if (index == 1)
        {
            leftKey = leftKey1;
            rightKey = rightKey1;
            frontKey = frontKey1;
        }
    }

    private void OnEnable()
    {
        transform.DOMoveY(-17, 1.5f)
                 .From()
                 .Play();
    }
}
