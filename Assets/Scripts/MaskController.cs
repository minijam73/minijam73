﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskController : MonoBehaviour
{
    public SpriteRenderer ColoredBackground;
    public Color MaskColor;
    public bool inUse;

    public CanyonController canyon;

    private List<GameObject> ID_obj = new List<GameObject> ();
    private SpriteMask _mask;

    private void Awake()
    {
        _mask = GetComponent<SpriteMask>();
        _mask.enabled = false;
        inUse = false;
        ID_obj.Clear();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Canyon"))
        {
            _mask.enabled = true;
            ColoredBackground.color = MaskColor;
            if(!inUse)
            {
                GetComponent<PolygonCollider2D>().enabled = false;
                GetComponent<PolygonCollider2D>().enabled = true;
                
            }
            inUse = true;


            //Physics2D.OverlapCollider(GetComponent<PolygonCollider2D>(), ContactFilter2D.NoFilter, );

            ColorChanger.instance.ChangeSpriteColor(MaskColor);
            canyon.SetLayer(gameObject.layer, MaskColor);
        }
        else if(collision.CompareTag("Asteroid"))
        {
            if(!ID_obj.Contains(collision.gameObject))
                ID_obj.Add(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Canyon") && inUse)
        {

            _mask.enabled = false;
            inUse = false;
            foreach(GameObject obj in ID_obj)
            {
                obj.GetComponentsInChildren<SpriteRenderer>()[1].enabled = false;
            }
            ID_obj.Clear();
        }
        else if(collision.CompareTag("Asteroid"))
        {
            ID_obj.Remove(collision.gameObject);
        }
    }

}
