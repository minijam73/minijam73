﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnToScreen : MonoBehaviour
{
    public float cameraOffset = 1f; // Tal vez haya que cambiarlo según el tamaño del asteroide

    private float minY, maxY, minX, maxX;

    private void Start()
    {
        minY = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, -Camera.main.transform.position.z)).y - cameraOffset;
        maxY = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, -Camera.main.transform.position.z)).y + cameraOffset;
        minX = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, -Camera.main.transform.position.z)).x - cameraOffset;
        maxX = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, -Camera.main.transform.position.z)).x + cameraOffset;
    }

    private void Update()
    {
        if (transform.position.x > maxX)
        {
            transform.position = new Vector2(minX, transform.position.y);
        }

        if (transform.position.x < minX)
        {
            transform.position = new Vector2(maxX, transform.position.y);
        }

        if (transform.position.y > maxY)
        {
            transform.position = new Vector2(transform.position.x, minY);
        }

        if (transform.position.y < minY)
        {
            transform.position = new Vector2(transform.position.x, maxY);
        }
    }
}
