﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SDBullet : MonoBehaviour
{
    public float speed;
    public float rotationSpeed;
    public float ShootDesv;
    private Transform _Target;
    private Vector2 _direction;
    // Start is called before the first frame update
    void Start()
    {
        CheckPlayer();
    }


    // Update is called once per frame
    void Update()
    {
        Rotate();
        goForward();
    }

    public void Rotate()
    {
        transform.Rotate(Vector3.forward * rotationSpeed * Time.deltaTime);
    }

    public void goForward()
    {
        transform.Translate(_direction * speed * Time.deltaTime);
    }
    private void CheckPlayer()
    {
        _Target = GameObject.FindObjectOfType<SpaceshipController>().gameObject.transform;
        _direction = (_Target.position - transform.position).normalized;
        _direction.x += Random.Range(-ShootDesv, ShootDesv);
        _direction.y += Random.Range(-ShootDesv, ShootDesv);
        _direction.Normalize();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Spaceship"))
        {
            collision.GetComponent<SpaceshipHealth>().TakeDamage();
        }
        else if(collision.CompareTag("Bullet"))
        {
            SFXController.instance.PlayRockBreak();

            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }
}
