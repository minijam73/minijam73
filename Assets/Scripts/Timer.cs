﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    public static Timer instance;

    public TMP_Text timerText;
    public TMP_Text finalTimeText;
    public TMP_Text bestTimeHeader;
    public TMP_Text bestTimeText;

    public int pointsPerSecond;

    private float startTime;
    private bool timerStarted;
    private float currentTime;
    private float finalTime;
    private string seconds;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        timerText.text = "0.00";
    }

    public void StartTimer()
    {
        startTime = Time.time;
        timerStarted = true;
        InvokeRepeating(nameof(IncreasePoints), 1, 1);
    }
    public void StopTimer()
    {
        timerStarted = false;
        CancelInvoke();
    }

    public void SetFinalTime()
    {
        StopTimer();
        finalTime = currentTime;
        finalTimeText.text = finalTime.ToString("f3");
        SetMaxTime();
    }

    public void SetMaxTime()
    {
        if (finalTime > PlayerPrefs.GetFloat("MaxTime", 0))
        {
            bestTimeHeader.text = "New Best Time!!!";
            bestTimeText.text = finalTime.ToString("f3");
            PlayerPrefs.SetFloat("MaxTime", finalTime);
        }
        else
        {
            bestTimeHeader.text = "Best Time";
            bestTimeText.text = GetMaxTime().ToString("f3");
        }
    }


    public float GetMaxTime()
    {
        return PlayerPrefs.GetFloat("MaxTime", 0);
    }

    void Update()
    {
        if (timerStarted)
        {
            currentTime = Time.time - startTime;

            seconds = (currentTime).ToString("f3");

            timerText.text = seconds;
        }
    }

    private void IncreasePoints()
    {
        ScoreController.instance.IncreaseScore(pointsPerSecond);
    }
}