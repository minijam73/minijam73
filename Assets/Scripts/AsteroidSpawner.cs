﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour
{
    public static AsteroidSpawner instance;

    public List<GameObject> asteroidPrefabs;
    public GameObject alienPrefab;
    public int minSize; // Must be the size of the prefabs
    public Transform asteroidParent;
    [Space]
    public List<DifficultyIncrease> difficultyLevels;

    private float spawnTime = 1;
    private float alienSpawnTime;
    private int maxSize;
    private int currentSize;
    private int currentDifficulty;
    private float minX, maxX, minY, maxY;
    private float cameraOffset = 0.25f;
    private float spawnX, spawnY;
    private Vector2 spawnPosition;
    private AsteroidBehaviour nextAsteroid;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        minY = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, -Camera.main.transform.position.z)).y - cameraOffset;
        maxY = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, -Camera.main.transform.position.z)).y + cameraOffset;
        minX = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, -Camera.main.transform.position.z)).x - cameraOffset;
        maxX = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, -Camera.main.transform.position.z)).x + cameraOffset;
    }

    public void Initialize()
    {
        currentDifficulty = 0;
        maxSize = minSize;

        Invoke(nameof(SpawnAsteroid), 1);
        Invoke(nameof(InvokeNewAlien), 1);
        Invoke(nameof(IncreaseDifficulty), difficultyLevels[currentDifficulty].timeToReach);
    }

    public void StopSpawn()
    {
        CancelInvoke();
    }

    private void SpawnAsteroid()
    {
        spawnPosition = GetSpawnPoint(false);
        nextAsteroid = Instantiate(asteroidPrefabs[Random.Range(0, asteroidPrefabs.Count)], spawnPosition, Quaternion.identity).GetComponent<AsteroidBehaviour>();

        nextAsteroid.size = currentSize;

        if (currentSize > minSize)
        {
            nextAsteroid.transform.localScale *= (2 * (currentSize - minSize));
        }

        nextAsteroid.transform.parent = asteroidParent;

        Invoke(nameof(SpawnAsteroid), spawnTime);
    }

    public void InvokeNewAlien()
    {
        Invoke(nameof(SpawnAlien), alienSpawnTime);
    }

    private void SpawnAlien()
    {
        spawnPosition = GetSpawnPoint(true);
        Instantiate(alienPrefab, spawnPosition, Quaternion.identity, asteroidParent);
    }

    private Vector2 GetSpawnPoint(bool isAlien)
    {
        int randomAxis = Random.Range(0, 2);
        int randomSide = Random.Range(0, 2);

        if (isAlien)
        {
            currentSize = 0;
        }
        else
        {
            currentSize = Random.Range(minSize, maxSize + 1);
        }

        // 0 = horizontal
        if (randomAxis == 0)
        {
            spawnY = Random.Range(minY, maxY);
            // 0 = izquierda, 1 = derecha

            if (randomSide == 0)
                spawnX = minX - currentSize;
            else
                spawnX = maxX + currentSize;
        }

        // 1 = vertical
        else
        {
            spawnX = Random.Range(minX, maxX);

            // 0 = abajo, 1 = arriba
            if (randomSide == 0)
                spawnY = minY - currentSize;
            else
                spawnY = maxY + currentSize;
        }

        return new Vector2(spawnX, spawnY);
    }

    private void IncreaseDifficulty()
    {
        maxSize = difficultyLevels[currentDifficulty].asteroidSize;
        spawnTime = difficultyLevels[currentDifficulty].spawnTime;
        alienSpawnTime = difficultyLevels[currentDifficulty].alienSpawnTime;

        currentDifficulty++;

        if (currentDifficulty < difficultyLevels.Count)
        {
            Invoke(nameof(IncreaseDifficulty), difficultyLevels[currentDifficulty].timeToReach);
        }
    }
}
