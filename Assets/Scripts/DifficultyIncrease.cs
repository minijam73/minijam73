﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DifficultyIncrease
{
    public string label;

    public float timeToReach;
    public int asteroidSize;
    public float spawnTime;
    public float alienSpawnTime;
}
