﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float moveSpeed;
    public float timeToDestroy;

    private void Update()
    {
        transform.Translate(Vector2.right * moveSpeed * Time.deltaTime);

        Destroy(gameObject, timeToDestroy);

    }
}
