﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreController : MonoBehaviour
{
    public static ScoreController instance;

    public TMP_Text finalScoreText;
    public TMP_Text currentScoreText;
    public TMP_Text highScoreHeader;
    public TMP_Text highScoreText;
    public int Multipler = 1;

    private int currentScore;
    private int finalScore;
    private bool canUpdate = true;

    private void Awake()
    {
        instance = this;
        UpdateUI();
    }

    public void IncreaseScore(int amount)
    {
        currentScore += amount * Multipler;
        UpdateUI();
    }


    public void ResetScore()
    {
        currentScore = 0;
        canUpdate = true;
        UpdateUI();
    }

    private void UpdateUI()
    {
        if (canUpdate)
        {
            currentScoreText.text = currentScore.ToString();
        }
    }

    public void SetFinalScore()
    {
        finalScore = currentScore;
        finalScoreText.text = finalScore.ToString();
        canUpdate = false;

        SetMaxScore();
    }

    public void SetMaxScore()
    {
        if (finalScore > PlayerPrefs.GetInt("MaxScore", 0)){
            highScoreHeader.text = "New High Score!!!";
            highScoreText.text = finalScore.ToString();
            PlayerPrefs.SetInt("MaxScore", finalScore);
        }
        else
        {
            highScoreHeader.text = "High Score";
            highScoreText.text = GetMaxScore().ToString();
        }
    }


    public int GetMaxScore()
    {
        return PlayerPrefs.GetInt("MaxScore", 0);
    }

    public int GetCurrentScore()
    {
        return currentScore;
    }

    public void DoubleMult(float time)
    {
        Multipler *= 2;
        Invoke(nameof(ReturnMult), time);
    }

    public void ReturnMult()
    {
        Multipler /= 2;
    }
}
