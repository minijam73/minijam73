﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidBehaviour : MonoBehaviour
{
    [Header("General")]
    public float rotationVariance;
    public float minSpeed;
    public float maxSpeed;
    public int size;
    public GameObject asteroidPrefab;
    public float childScale;

    public AsteroidType type;
    [Header("Score")]
    public int baseScore;
    [Header("SFX")]
    public AudioSource AS_break;
    public AudioClip Clip_break;

    private Transform asteroidParent;
    private float rotationSpeed;
    private float currentSpeed;
    private Vector2 upVector;
    private Vector2 rightVector;
    protected Vector2 direction;

    private AsteroidBehaviour currentAsteroid;

    private void Awake()
    {
        if (!GetComponent<AsteroidBehaviour>().isActiveAndEnabled)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        Initialize();
        GetComponent<ReturnToScreen>().cameraOffset = 0.5f + 1.25f * (size - 1);
        asteroidParent = GameObject.FindGameObjectWithTag("AsteroidParent").transform;
    }

    private void Update()
    {
        Move();
        Rotate();
    }

    protected virtual void Initialize()
    {
        rotationSpeed = Random.Range(-rotationVariance, rotationVariance);
        currentSpeed = Random.Range(minSpeed, maxSpeed);


        rightVector = Vector2.right * Random.Range(-0.8f, 0.8f);

        if (rightVector == Vector2.zero)
        {
            rightVector = Vector2.right;
        }


        upVector = Vector2.up * Random.Range(-0.8f, 0.8f);

        if (upVector == Vector2.zero)
        {
            upVector = Vector2.up;
        }


        direction = (rightVector + upVector).normalized;
    }

    protected virtual void Move()
    {
        transform.Translate(direction * currentSpeed * Time.deltaTime, Space.World);
    }

    protected virtual void Rotate()
    {
        transform.Rotate(Vector3.forward * rotationSpeed * Time.deltaTime);
    }

    private void GetHit()
    {
        ScoreController.instance.IncreaseScore(baseScore * size);

        SFXController.instance.PlayRockBreak();

        if (size > 1)
        {
            CreateAsteroid();
            CreateAsteroid();
        }

        Destroy(gameObject);
    }

    private void CreateAsteroid()
    {
        currentAsteroid = Instantiate(asteroidPrefab, transform.position, Quaternion.identity).GetComponent<AsteroidBehaviour>();
        currentAsteroid.transform.parent = asteroidParent;
        currentAsteroid.size = size - 1;
        currentAsteroid.transform.localScale *= childScale;
        currentAsteroid.GetComponent<ReturnToScreen>().cameraOffset = 0.5f + 1.25f * (size - 1);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet"))
        {
            GetHit();
            Destroy(collision.gameObject);
        }
        else if (collision.CompareTag("Spaceship"))
        {
            collision.GetComponent<SpaceshipHealth>().TakeDamage();
        }
    }
}