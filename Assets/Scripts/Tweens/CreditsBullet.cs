﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CreditsBullet : MonoBehaviour
{
    public GameObject text;
    public float minStrengh;
    public float maxStrengh;
    //public float timeToHide;
    public GameObject target;

    private float timer;
   // private float hidetimer;
    private bool started;
    private Vector2 startPos;
    private float timeToNextShot;
    private float speed;

    private void Awake()
    {
        started = false;
        startPos = transform.localPosition;
        speed = 600f; 
    }

    private void Update()
    {
        if (started)
        {
            BulletAnimation();

            if(Vector2.Distance(transform.position, target.transform.position) <= 5f)
            {
                transform.localPosition = startPos;
                ShakeText();
                //hidetimer = 0;
                started = false;
            }
            /*
            hidetimer += Time.deltaTime;
            if(hidetimer >= timeToHide)
            {
                transform.localPosition = startPos;
                ShakeText();
                hidetimer = 0;
                started = false;
            }*/
        }
        else
        {
            timer += Time.deltaTime;
            if(timer >= timeToNextShot)
            {
                started = true;
                timer = 0;
            }
        }
    }

    public void BulletAnimation()
    {
        transform.Translate(Vector2.up * speed * Time.deltaTime);
    }

    void ShakeText()
    {
        text.transform.DOShakePosition(0.5f, Random.Range(minStrengh, maxStrengh))
            .Play();

        timeToNextShot = Random.Range(2f, 6f);
        speed = Random.Range(400f, 600f);
    }
}
