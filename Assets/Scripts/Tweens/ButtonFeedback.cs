﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ButtonFeedback : MonoBehaviour
{
    public float scaleValue;

    public void MouseEnter()
    {
        transform.DOScale(Vector3.one * scaleValue, 0.1f)
            .SetEase(Ease.InQuad)
            .SetUpdate(true)
            .Play();
    }

    public void MouseExit()
    {
        transform.DOScale(Vector3.one, 0.1f)
            .SetEase(Ease.InQuad)
            .SetUpdate(true)
            .Play();
    }
}
