﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerPowerUps : MonoBehaviour
{
    public static SpawnerPowerUps instance;
    public List<GameObject> PowerUps;
    private float timer = 0;
    public float TimeToAppear = 60;
    //public float duration;
    private void Awake()
    {
        instance = this;
        DeactivateAll();
    }

    void Update()
    {
        timer += Time.deltaTime;
        if(timer>= TimeToAppear)
        {
            timer = 0;
            if(Random.value<0.9f)
            {
                int rand = Random.Range(0, 4);
                PowerUps[rand].SetActive(true);
            }
        }
    }

    public void DeactivateAll()
    {
        foreach (GameObject obj in PowerUps)
        {
            //GetComponent<PowerUp>().duration = duration;
            obj.SetActive(false);
        }
    }
}
