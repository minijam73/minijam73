﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXController : MonoBehaviour
{
    public static SFXController instance;
    [Header("AudioSource")]
    public AudioSource RockBreak;
    public AudioSource Shoot;
    public AudioSource Hit;
    public AudioSource Alien;

    [Header("AudioClips")]
    public AudioClip C_Rock;
    public AudioClip C_Shoot;
    public AudioClip C_Hit;
    public AudioClip C_PUp;
    public AudioClip C_AlienAppear;
    public AudioClip C_AlienHit;

    [Header("Pitch Variation")]
    public float PitchBias;
    public float PitchRandomRange;

    private void Awake()
    {
        instance = this;
    }

    public void PlayShoot()
    {
        Shoot.pitch = Random.Range(PitchBias - PitchRandomRange, PitchBias + PitchRandomRange);
        Shoot.PlayOneShot(C_Shoot);
    }

    public void PlayRockBreak()
    {
        RockBreak.PlayOneShot(C_Rock);
    }

    public void PlayHit()
    {
        Hit.PlayOneShot(C_Hit);
    }
    public void PlayPUp()
    {
        Hit.PlayOneShot(C_PUp);
    }

    public void PlayAlienAppear()
    {
        Alien.PlayOneShot(C_AlienAppear);
    }

    public void PlayAlienHit()
    {
        Alien.PlayOneShot(C_AlienHit);
    }
}
