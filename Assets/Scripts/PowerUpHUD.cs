﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PowerUpHUD : MonoBehaviour
{
    public float duration;
    void Start()
    {
        //Invoke(nameof(DoAnim), duration - 1f);
        //Invoke(nameof(Disappear), duration);
    }
    private void OnEnable()
    {
        CancelInvoke();
        Invoke(nameof(DoAnim), duration - 1f);
        Invoke(nameof(Disappear), duration);
    }
    private void DoAnim()
    {
        GetComponent<Animator>().SetTrigger("Blink");
    }

    private void Disappear()
    {
        gameObject.SetActive(false);
    }

}
