﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ColorChanger : MonoBehaviour
{
    public static ColorChanger instance;

    public Color currentColor;

    public List<SpriteRenderer> listOfSprites;
    public List<Image> listOfImages;
    public List<TMP_Text> listOfTMP;

    private void Awake()
    {
        if (!instance) { instance = this; }
        else { Destroy(gameObject); }
    }

    public void ChangeSpriteColor(Color col)
    {
        currentColor = col;

        foreach (SpriteRenderer sprite in listOfSprites)
        {
            sprite.color = col;
        }

        foreach (Image image in listOfImages)
        {
            image.color = col;
        }

        foreach (TMP_Text text in listOfTMP)
        {
            text.color = col;
        }
    }
}
